from flask import Flask

app = Flask(__name__)


@app.route('/greet/<name>')
def greet_user(name: str):
    print(f"Gteetings {name}")
    return f"Hello {name}!"


def main():
    app.run(host="0.0.0.0", port=8000)


if __name__ == '__main__':
    main()
